#include <iostream>

class Stack
{
public:
    Stack(int _size) : size(_size)
    {}

    void push(int newElement)
    {
        if (topIndex < size - 1)        //Check if we can add new element
        {
            topIndex++;
            StackArray[topIndex] = newElement;
        }

    }

    void pop()
    {
        if (topIndex != -1)
        {
            StackArray[topIndex] = 0;
            topIndex--;
        }
    }

    //Return any element of Stack
    int get(int index)
    {
        if ((index > 0) && (index <= topIndex + 1))
        {
            return StackArray[index - 1];
        }
        else
        {
            return 0;
        }
    }

    //Print all elements of Stack
    void show()
    {
        for (int i = 0; i <= topIndex; i++)
        {
            std::cout << i <<" " << StackArray[i] << "\n";
        }
    }

private:
    int size;
    int topIndex = -1;
    int* StackArray = new int[size];

};

int main()
{
    //Test stack
    Stack st(4);
    st.push(12);
    st.push(15);
    st.push(-6);

    st.show();

    st.pop();
    std::cout << "\n";
    st.show();

    st.push(47);
    std::cout << "\n";
    st.show();

    st.push(351);
    std::cout << "\n";
    st.show();

    st.pop();
    st.pop();
    st.pop();
    std::cout << "\n";
    st.show();

    st.pop();
    std::cout << "\n";
    st.show();
}